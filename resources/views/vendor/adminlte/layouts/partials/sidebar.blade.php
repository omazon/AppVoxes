<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li {{ (Request::is('home') ? 'class=active' : '') }}>
                <a href="{{ url('home') }}">
                    <i class='fa fa-link'></i>
                    <span>{{ trans('adminlte_lang::message.home') }}</span>
                </a>
            </li>
            <li {{ (Request::is('home/country') ? 'class=active' : '') }}>
                <a href="{{url('home/country')}}">
                    <i class='fa fa-flag'></i>
                    <span>Países</span>
                </a>
            </li>
            <li {{ (Request::is('home/subscriber') ? 'class=active' : '') }}>
                <a href="{{url('home/subscriber')}}">
                    <i class='fa fa-users'></i>
                    <span>Suscriptores</span>
                </a>
            </li>
            <li {{ (Request::is('home/message') ? 'class=active' : '') }}>
                <a href="{{url('home/message')}}">
                    <i class='fa fa-users'></i>
                    <span>Mensajes</span>
                </a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

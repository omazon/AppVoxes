<link rel="stylesheet" href="https://cdn.datatables.net/v/bs/jq-2.2.4/dt-1.10.13/datatables.min.css">
<script src="https://cdn.datatables.net/v/bs/jq-2.2.4/dt-1.10.13/datatables.min.js" type="text/javascript"></script>
<script>
    $(function(){
        $("#data_table").DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            }
        });
    })
</script>
<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>App Voxes-Envía tu mensaje</title>

    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/all-landing.css') }}" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

</head>

<body data-spy="scroll" data-target="#navigation" data-offset="50">

<div id="app">
    {{--inicio cuarpo--}}
    <div class="container">
        <h1>Envía tu mensaje</h1>
        <div class="panel panel-default">
            <div class="panel-header">
                @include('flash::message')
                @if(count($errors)>0)
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="panel-body">
                {!! Form::open(['route'=>'message.store','method'=>'POST','class'=>'form-group']) !!}
                <div class="form-group">
                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingrese el título de su mensaje','required'=>'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::textarea('body',null,['class'=>'form-control','placeholder'=>'Ingrese el cuerpo de su mensaje','required'=>'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Enviar mensaje',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--fin cuerpo--}}
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ mix('/js/app-landing.js') }}"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
</body>
</html>

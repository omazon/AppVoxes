@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
Sección principal
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Bienvenido</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						Administración de usuarios
                        @include('flash::message')
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				<div class="row">
					<div class="table-responsive">
						<table class="table table-striped" id="data_table">
							<thead>
							<tr>
								<th>Nombre</th>
								<th>Email</th>
								<th>Fecha de creación</th>
								<th>Acciones</th>
							</tr>
							</thead>
							<tbody>
							@foreach($users as $user)
								<tr>
									<td>
										{{$user->name}}
									</td>
									<td>
										{{$user->email}}
									</td>
									<td>
										{{$user->created_at}}
									</td>
                                    <td>
                                        {!! Form::open(['route'=>['user.destroy',$user->id],'method'=>'DELETE']) !!}
                                        {!! Form::submit('Eliminar',['class'=>'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SubscriberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $index){
            DB::table('subscribers')->insert([
                'email'=>$faker->email,
                'country_id'=>$faker->numberBetween($min = 1, $max = 10),
            ]);
        }
    }
}

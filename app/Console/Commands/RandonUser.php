<?php

namespace App\Console\Commands;

use App\RandonUrl;
use App\Subscriber;
use Illuminate\Console\Command;
use Illuminate\Contracts\Logging\Log;
use App\Mail\DailyChoose;
use Illuminate\Support\Facades\Mail;

class RandonUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:randon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Borrar los usuarios con status isDelete 1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        selecciona todos los usuarios con select 1 y los pasa a 0, luego selecciona uno al azar y lo pone a 1
        Subscriber::where('select',1)->update(['select'=>0]);
        $subscriber = Subscriber::inrandomOrder()->first();
        $subscriber -> select = 1;
        $subscriber->save();
        //        selecciona las url con estatus 1 y las pasa a 0, luego genera una url al azar y le asiga a 1
        RandonUrl::where('status',1)->update(['status'=>0]);
        RandonUrl::create(['name'=>$this->generateRandomString(),'status'=>1,'subscriber_id'=>$subscriber->id]);
        $randon = RandonUrl::where('status',1)->get();
        Mail::to($subscriber)->send(new DailyChoose($randon));
    }
    function generateRandomString($length = 60) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

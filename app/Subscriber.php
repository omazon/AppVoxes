<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $fillable = [
        'email','country_id','select'
    ];

    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function message(){
        return $this->hasMany(Message::class);
    }
    public function randon_url(){
        return $this->hasMany(RandonUrl::class);
    }
}

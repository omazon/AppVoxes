<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RandonUrl extends Model
{
    protected $fillable = [
        'name','status','subscriber_id'
    ];
    public function subscriber(){
        return $this->belongsTo(Subscriber::class);
    }
}

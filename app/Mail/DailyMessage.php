<?php

namespace App\Mail;

use App\RandonUrl;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DailyMessage extends Mailable
{
    use Queueable, SerializesModels;
    public $randonUrl;

    /**
     * Create a new message instance.
     *
     * @param RandonUrl $randon
     */
    public function __construct(RandonUrl $randonUrl)
    {
        $this->randonUrl = $randonUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.select');
    }
}

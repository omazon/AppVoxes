<?php

namespace App\Http\Controllers;

use App\Country;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()) {
            $subscribers = Subscriber::all();
            return view('vendor.adminlte.subscriber')->with('subscribers', $subscribers);
        }else{
            return redirect('login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Subscriber::create($request->all());
        flash('Te has suscrito exitosamente','success')->important();
        return back()->withInput();
    }
}

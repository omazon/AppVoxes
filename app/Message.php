<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable=[
      'name','body','subscriber_id','publish'
    ];
    public function subscriber(){
        return $this->belongsTo(Subscriber::class);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\RandonUrl;
Route::resource('/','LandingController');

Route::group(['middleware'=>'guest'],function(){
    Route::resource('subscriber', 'SubscriberController');
});
Route::group(['middleware' => 'auth','prefix'=>'home'], function () {
    Route::resource('country','CountryController');
    Route::get('subscriber', 'SubscriberController@index');
    Route::resource('user','UserController');
    Route::resource('logs','LogsController');
    Route::get('email',function(){
       $data = [
           'title'=>'Fuistes elegido',
           'content'=>'Has sido seleccionado para enviar un mensaje'
       ];
       Mail::send('email.select',$data,function($message){
          $message->to('omarboza@gmail.com','Omar')->subject('Seleccionado');
       });
    });
    Route::resource('message','MessageController');
});

//ruta para el editor
Route::get('/{url}',function ($url){
//    url activo
    $url_active = RandonUrl::where('status',1)->value('name');
    if($url == $url_active){

        return view('vendor.adminlte.layouts.message.send');
    }
//    valida si la url existe en la base de datos
    if(RandonUrl::where('name','=',$url)->count() > 0){
        return 'url expirado';
    }
    else{
        return 'error 404';
    }

});